<?php
namespace Mini\Exceptions;

/**
 * HttpExceptionInterface
 *
 * An interface for type-hinting generic HTTP errors
 */
interface HttpExceptionInterface extends MiniExceptionInterface
{
}
