<?php
namespace Mini\Exceptions;

use RuntimeException;

/**
 * UnhandledException
 *
 * Exception used for when a exception isn't correctly handled by the Mini error callbacks
 */
class UnhandledException extends RuntimeException implements MiniExceptionInterface
{
}
